using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    private CubePos nowCube = new CubePos(0, 1, 0);
    public float cubeChangePlaceSpeed = 0.5f;
    public Transform cubeToPlase;
    private float CamMoveToYpos, camMoveSpeed = 2f;
    private Transform mainCam;
    private int prevCountMaxHorizontal = 0;

    public Text scoreTxt;

    public GameObject[] cubesToCreate;
    private List<GameObject> posibleCubesToCreate = new List<GameObject>();

    public GameObject AllCubes, vfx;
    private Rigidbody allCubesRb;

    private bool IsLose, FirstCube;

    private Coroutine showCubePlace;
    public GameObject[] canvasStartPage;

    public Color[] bgColors;
    private Color toCameraColor;

    private List<Vector3> allCubePosition = new List<Vector3> {
      new Vector3(0, 0, 0),
      new Vector3(1, 0, 0),
      new Vector3(-1, 0, 0),
      new Vector3(0, 1, 0),
      new Vector3(0, 0, -1),
      new Vector3(1, 0, 1),
      new Vector3(-1, 0, -1),
      new Vector3(-1, 0, 1),
      new Vector3(1, 0, -1),
    };

    private void Start()
    {
        if(PlayerPrefs.GetInt("score") < 5)
        {
            posibleCubesToCreate.Add(cubesToCreate[0]);
        }
        else if(PlayerPrefs.GetInt("score") < 10)
        {
            AddPossibleCubes(2);
        }
        else if (PlayerPrefs.GetInt("score") < 15)
        {
            AddPossibleCubes(3);
        }
        else if (PlayerPrefs.GetInt("score") < 25)
        {
            AddPossibleCubes(4);
        }
        else if (PlayerPrefs.GetInt("score") < 35)
        {
            AddPossibleCubes(5);
        }
        else if (PlayerPrefs.GetInt("score") < 50)
        {
            AddPossibleCubes(6);
        }
        else if (PlayerPrefs.GetInt("score") < 70)
        {
            AddPossibleCubes(7);
        }
        else if (PlayerPrefs.GetInt("score") < 90)
        {
            AddPossibleCubes(8);
        }
        else if (PlayerPrefs.GetInt("score") < 110)
        {
            AddPossibleCubes(9);
        }
        else 
        {
            AddPossibleCubes(10);
        }

        scoreTxt.text = $"<size=40><color=#413434>best:</color></size> {PlayerPrefs.GetInt("score")}\n<size=27> now:</size> 0";
        toCameraColor = Camera.main.backgroundColor;
        mainCam = Camera.main.transform;
        CamMoveToYpos = 5.9f + nowCube.y - 1f;

        allCubesRb = AllCubes.GetComponent<Rigidbody>();
        showCubePlace =  StartCoroutine(ShowCubePlace());
    }

    private void Update()
    {
        if ((Input.GetMouseButtonDown(0) || Input.touchCount > 0) && cubeToPlase != null && AllCubes != null)
        {
#if !UNITY_EDITOR
        if(Input.GetTouch(0).phase != TouchPhase.Began)
            return;

             if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                Debug.Log("Pres UI");
                return;
            }
#endif

#if UNITY_EDITOR
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Pres UI");
                return;
            }
#endif

            Debug.Log("Touch free place");

            if (!FirstCube)
            {
                FirstCube = true;
                foreach(GameObject obj in canvasStartPage)
                {
                     Destroy(obj);
                }
            }

            GameObject createCube = null;
            if (posibleCubesToCreate.Count == 1)
                createCube = posibleCubesToCreate[0];
            else
                createCube = cubesToCreate[UnityEngine.Random.Range(0, posibleCubesToCreate.Count)];

            GameObject newCube = Instantiate(createCube, cubeToPlase.position, Quaternion.identity) as GameObject;

            newCube.transform.SetParent(AllCubes.transform);
            nowCube.setVector(cubeToPlase.position);
            allCubePosition.Add(nowCube.getVector());

            if (PlayerPrefs.GetString("music") != "No")
                GetComponent<AudioSource>().Play();

            GameObject newVfx = Instantiate(vfx, cubeToPlase.position, Quaternion.identity) as GameObject;
            Destroy(newVfx, 1.5f);

            allCubesRb.isKinematic = true;
            allCubesRb.isKinematic = false;

            SpawnPosition();
            MoveCameraChangeBG();
        }       

        if(!IsLose && allCubesRb.velocity.magnitude > 0.1f)
        {
            Destroy(cubeToPlase.gameObject);
            IsLose = true;
            StopCoroutine(showCubePlace);
        }

        mainCam.localPosition = Vector3.MoveTowards(mainCam.localPosition, 
            new Vector3(mainCam.localPosition.x, CamMoveToYpos, mainCam.localPosition.z), camMoveSpeed * Time.deltaTime);

        if (Camera.main.backgroundColor != toCameraColor)
            Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, toCameraColor, Time.deltaTime / 1.5f);
    }

    IEnumerator ShowCubePlace()
    {
        while (true)
        {
            SpawnPosition();

            yield return new WaitForSeconds(cubeChangePlaceSpeed);
        }
    }

    private void SpawnPosition()
    {
        List<Vector3> positions = new List<Vector3>();

        if (IsPositionEmpy(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z)) && nowCube.x + 1 != cubeToPlase.position.x)
        {
            positions.Add(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z) );
        } 
        if(IsPositionEmpy(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z)) && nowCube.x - 1 != cubeToPlase.position.x)
        {
            positions.Add(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z));
        }
        if (IsPositionEmpy(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z)) && nowCube.y + 1 != cubeToPlase.position.y)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z));
        }
        if (IsPositionEmpy(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z)) && nowCube.y - 1 != cubeToPlase.position.y)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z));
        }
        if (IsPositionEmpy(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1)) && nowCube.z + 1 != cubeToPlase.position.z)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1));
        }
        if (IsPositionEmpy(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1)) && nowCube.z - 1 != cubeToPlase.position.z)
        {
            positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1));
        }

        if (positions.Count > 1)
            cubeToPlase.position = positions[UnityEngine.Random.Range(0, positions.Count)];
        else if (positions.Count == 0)
            IsLose = true;
        else
            cubeToPlase.position = positions[0];
    }

    private bool IsPositionEmpy(Vector3 targetPos)
    {
        if(targetPos.y == 0)
        {
            return false;
        }

        foreach (Vector3 pos in allCubePosition)
        {
            if (pos.x == targetPos.x && pos.y == targetPos.y && pos.z == targetPos.z)
                return false;
        }
        return true;
    }

    private void MoveCameraChangeBG()
    {
        int maxX = 0, maxY = 0, maxZ = 0, maxHor;

        foreach(Vector3 pos in allCubePosition)
        {
            if(Mathf.Abs(Convert.ToInt32(pos.x)) > maxX)
            {
                maxX = Convert.ToInt32(pos.x);
            }

            if (Mathf.Abs(Convert.ToInt32(pos.y)) > maxY)
            {
                maxY = Convert.ToInt32(pos.y);
            }

            if (Mathf.Abs(Convert.ToInt32(pos.z)) > maxZ)
            {
                maxZ = Convert.ToInt32(pos.z);
            }
        }

        if (PlayerPrefs.GetInt("score") < maxY -1)
            PlayerPrefs.SetInt("score", maxY - 1);

        scoreTxt.text = $"<size=40><color=#413434>best:</color></size> {PlayerPrefs.GetInt("score")}\n<size=27> now:</size> {maxY - 1}";

        CamMoveToYpos = 5.9f + nowCube.y - 1f;

        maxHor = maxX > maxZ ? maxX : maxZ;
        if(maxHor % 3 == 0 && prevCountMaxHorizontal != maxHor)
        {
            mainCam.localPosition -= new Vector3(0, 0, 2.5f);
            prevCountMaxHorizontal = maxHor;
        }

        if (maxY >= 80)
            toCameraColor = bgColors[7];
        else if (maxY >= 70)
            toCameraColor = bgColors[6];
        else if(maxY >= 60)
            toCameraColor = bgColors[5];
        else if (maxY >= 50)
            toCameraColor = bgColors[4];
        else if (maxY >= 40)
            toCameraColor = bgColors[3];
        else if (maxY >= 30)
            toCameraColor = bgColors[2];
        else if (maxY >= 20)
            toCameraColor = bgColors[1];
        else if (maxY >= 10)
            toCameraColor = bgColors[0];
    }

    private void AddPossibleCubes(int till)
    {
        for (int i = 0; i < till; i++)
        {
            posibleCubesToCreate.Add(cubesToCreate[i]);
        }
    }
}


struct CubePos
{
    public int x, y, z;
    public CubePos(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3 getVector()
    {
        return new Vector3(x, y, z);
    }

    public void setVector(Vector3 pos)
    {
        x = Convert.ToInt32(pos.x);
        y = Convert.ToInt32(pos.y);
        z = Convert.ToInt32(pos.z);
    }
}